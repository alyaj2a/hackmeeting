
$(document).ready(function(){
  $('.btn-sidebar').click(function(){
    $('.sidebar').toggleClass('active');
    $('.btn-sidebar').toggleClass('toggle');
  })
})
$(document).ready(function(){
  $("#btn-abrir-menu").click(function(){
    if($(this).hasClass('open')){
      $(this).removeClass('open');
      $("#menu li:not(.logo)").removeClass('open');
    }
    else{
      $(this).addClass('open');
      $("#menu li:not(.logo)").addClass('open');
      $("a#tab").click(function(){
        $("#menu li:not(.logo)").removeClass('open');
        $("#btn-abrir-menu").removeClass('open');
      })
    }
  })
  $('a[href^="#"]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      && location.hostname == this.hostname) {
        var $target = $(this.hash);
        $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
        if ($target.length) {
          var targetOffset = $target.offset().top;
          $('html,body').animate({scrollTop: targetOffset}, 1000);
          return false;
        }
      }
  });
})
